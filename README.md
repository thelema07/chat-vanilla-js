# Chat Vanilla JS

Chat Application using Javascript => HTML, CSS, JS & NodeJS

### The What

- Small chat app, using socket.io as WebSocket framework
- Demonstrate the creation of rooms and users
- Code in Modern JS

### The How

Wanna test?

- run "npm run dev" on console
- Go to Localhost:3000
- Voilá, just provide feedback

### The Who (Not the band)

- Daniel Felipe Mesu
- danielrojo1927@gmail.com
